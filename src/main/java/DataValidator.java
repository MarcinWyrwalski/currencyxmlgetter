import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DataValidator {

    String data;

    public boolean isDateProper(String str){

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        try {
            Date date = dateFormat.parse(str);
            return true;
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("Błedna data: "+str);
        }
        return false;
    }

    public boolean isDateValuesinProperOrder(String date1, String date2){
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        Date firstDate =null;
        Date secondDate=null;


        try {
             firstDate = dateFormat.parse(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
             secondDate = dateFormat.parse(date2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (firstDate.before(secondDate)){
            return true;
        } else {
            System.out.println("First date is not bigger that second! Exiting program");
            System.exit(4);

        }
        return false;
    }

}
