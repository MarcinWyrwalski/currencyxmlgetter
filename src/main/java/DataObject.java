import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataObject {

    List<CurrencyObject> currencyObjects = new ArrayList<>();

    public DataObject() throws ParserConfigurationException, SAXException, IOException {
    }

    public boolean isValueProper(String str) {

        for (int i = 0; i < currencyObjects.size(); i++) {
            if (currencyObjects.get(i).kod_waluty.equals(str)) {
                System.out.println("znaleziono: " + str);
                return true;
            }
        }
        return false;
    }
}
