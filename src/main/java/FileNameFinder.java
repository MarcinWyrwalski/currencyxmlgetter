import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class FileNameFinder {

    public String regexBulider(String date) {
        StringBuilder result = new StringBuilder();
        result.append("c.*" + date + ".*");

//        System.out.println(result);
        return result.toString();
    }

    public String isFileExist(String path, String dateTofind) {
        BufferedReader bufferedReader = null;
        String name = null;
        String[] words = null;
        try {
            URL url = new URL(path);
            bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));

            String currentLine;
            FileNameFinder fnf = new FileNameFinder();
            String regExToFind = fnf.regexBulider(dateTofind);

            while ((currentLine = bufferedReader.readLine()) != null) {
                if (currentLine.matches(regExToFind)) {
                    System.out.println(" znaleziono");
                    words = currentLine.split(" ");

                    for (int i = 0; i < words.length; i++) {
                        return words[0];
                    }
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String pathBuliderForFileFinder(String name) throws ParseException {
        String baseforPathBulider = "https://www.nbp.pl/kursy/xml/dir";
        String endForPathBulider = ".txt";

        return bulidPathFromStrings(name, baseforPathBulider, endForPathBulider);
    }

    public String pathBuliderForFileGettrr(String name) throws ParseException {

        StringBuilder sb = new StringBuilder();
        String baseforFileGEtter = "https://www.nbp.pl/kursy/xml/";
        String endForFileGEtter = ".xml";

        sb.append(baseforFileGEtter);
        sb.append(name);
        sb.append(endForFileGEtter);

        return sb.toString();
    }

    private String bulidPathFromStrings(String name, String baseforPathBulider, String endForPathBulider) throws ParseException {
        StringBuilder result = new StringBuilder();
        DateConverter dateConverter = new DateConverter();

        if (dateConverter.isYearCurrent("2019")) {

            result.append(baseforPathBulider);
            result.append(endForPathBulider);
        } else {
            result.append(baseforPathBulider);
            result.append(name);
            result.append(endForPathBulider);
        }
        return result.toString();
    }
}
