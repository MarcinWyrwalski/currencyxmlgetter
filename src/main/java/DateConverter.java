import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateConverter extends Date {

    public boolean isYearCurrent(String str) throws ParseException {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");

        Date currentTime = new Date();
        Calendar instanceOne = Calendar.getInstance();
        Calendar instanceTwo = Calendar.getInstance();
        instanceOne.setTime(simpleDateFormat.parse(str));
        instanceTwo.setTime(currentTime);

        if (instanceOne.get(Calendar.YEAR) == instanceTwo.get(Calendar.YEAR)) {

            return true;
        }
        return false;
    }

    public Date convertToDate(String str) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse(str);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.getTime();
    }

    public Date incrementByOneDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);
        return calendar.getTime();
    }

    public String getYearFromDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.get(Calendar.YEAR);
        int result = calendar.get(Calendar.YEAR);
        return String.valueOf(result);
    }

    public String parseDateToString(Date date) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyMMdd");
        String str = simpleDateFormat.format(date);
        return str;
    }

    public String convertTostringFormat(String firstDate) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = simpleDateFormat.parse(firstDate);
        SimpleDateFormat newDateFormat = new SimpleDateFormat("yyMMdd");
        String resultDate = newDateFormat.format(date);

        return resultDate;
    }
}
