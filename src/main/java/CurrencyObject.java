import java.math.BigDecimal;
import java.util.List;

public class CurrencyObject {

    String nazwa_waluty,przelicznik,kod_waluty,kurs_kupna,kurs_sprzedazy;

    public CurrencyObject(String nazwa_waluty, String przelicznik, String kod_waluty, String kurs_kupna, String kurs_sprzedazy) {
        this.nazwa_waluty = nazwa_waluty;
        this.przelicznik = przelicznik;
        this.kod_waluty = kod_waluty;
        this.kurs_kupna = kurs_kupna;
        this.kurs_sprzedazy = kurs_sprzedazy;
    }

    public static void displayResult(List<CurrencyObject> result, String currencyAcronim) {
        float srednia = 0;



        System.out.println("Kod waluty: "+currencyAcronim);
        for (CurrencyObject co: result
             ) {
            if (co.kod_waluty.equals(currencyAcronim)){
                srednia += Float.valueOf(co.kurs_kupna);
            }
        }
        float result1 = srednia/currencyAcronim.length();
        System.out.println(result1);

    }

    public String getNazwa_waluty() {
        return nazwa_waluty;
    }

    public String getPrzelicznik() {
        return przelicznik;
    }

    public String getKod_waluty() {
        return kod_waluty;
    }

    public String getKurs_kupna() {
        return kurs_kupna;
    }

    public String getKurs_sprzedazy() {
        return kurs_sprzedazy;
    }
}
