import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;

public class Main {

    public static void main(String[] args) throws ParseException, IOException, SAXException, ParserConfigurationException {

        String beginningDate = "24.01.2019";
        String endDate = "05.02.2019";
        String currencyAcronym = "USD";

//        CurrencyCodeValidator ccv = new CurrencyCodeValidator();
//        ccv.isCurrencyAcronimProper(currencyAcronym);




//        if (args.length == 3) {
//            beginningDate = args[0];
//            endDate = args[1];
//            currencyAcronym = args[2];
//
//            System.out.println(beginningDate);
//            System.out.println(endDate);
//            System.out.println(currencyAcronym);
//        } else {
//            System.out.println("błąd !" + args.length);
//        }

        ExchangeControler ex = new ExchangeControler();
        ex.start(beginningDate, endDate, currencyAcronym);
    }
}
