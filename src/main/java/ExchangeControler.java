import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

public class ExchangeControler {

    public void start(String firstDate, String secondDate, String currencyAcronim) throws ParserConfigurationException, SAXException, IOException, ParseException {

        DataValidator dataValidator = new DataValidator();
        CurrencyCodeValidator currencyCodeValidator = new CurrencyCodeValidator();

        if (dataValidator.isDateProper(firstDate)
                && dataValidator.isDateProper(secondDate)
                && currencyCodeValidator.isCurrencyAcronimProper(currencyAcronim)
                && dataValidator.isDateValuesinProperOrder(firstDate, secondDate)) {

//            executeProgram(firstDate, secondDate, currencyAcronim);
           List<CurrencyObject> result = dateLoop(firstDate, secondDate);
           CurrencyObject.displayResult(result, currencyAcronim);
        } else {
            System.out.println("Improper data!. Programs will exit");
            System.exit(6);
        }
    }

    private List<CurrencyObject> dateLoop(String firstDate, String secondDate) throws ParseException, IOException, SAXException, ParserConfigurationException {
        DateConverter first = new DateConverter();
        DateConverter second = new DateConverter();

        Date startDate = first.convertToDate(firstDate);
        Date endDate = second.convertToDate(secondDate);

        DateConverter dateConverter = new DateConverter();
        FileNameFinder fnf = new FileNameFinder();
        List<CurrencyObject> resultList = new ArrayList<>();

        do {

            String convertedStringToDate = dateConverter.convertTostringFormat(firstDate);
            String regexData = fnf.regexBulider(convertedStringToDate);

            String pathForFile = fnf.pathBuliderForFileFinder(regexData);

            String fileNameFromWeb = fnf.isFileExist(pathForFile, convertedStringToDate);
            String fileNameFOrFileGetter = fnf.pathBuliderForFileGettrr(fileNameFromWeb);


            XMLFileGeter xmlFileGeter1 = new XMLFileGeter();
            Document doc = xmlFileGeter1.getFile(fileNameFOrFileGetter);

            XMLFIleInterpreter xmlfIleInterpreter1 = new XMLFIleInterpreter(doc);

            List<CurrencyObject> currencyObjects = xmlfIleInterpreter1.XMLFIleInterpreter(doc);
            resultList.addAll(currencyObjects);
            currencyObjects.clear();

            startDate = unceremntDateByOneDay(startDate);
        } while (startDate.before(endDate));

        return resultList;
    }

    private Date unceremntDateByOneDay(Date f) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(f);
        calendar.add(Calendar.DATE, 1);
        f = calendar.getTime();
        return f;
    }
}
