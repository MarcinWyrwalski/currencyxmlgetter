import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CurrencyCodeValidator {

    public boolean isCurrencyAcronimProper(String str) throws IOException, SAXException, ParserConfigurationException {

        XMLFileGeter docum = new XMLFileGeter();
        Document document = docum.getFile("http://www.nbp.pl/kursy/xml/LastC.xml");

        XMLFIleInterpreter fileInterpreter = new XMLFIleInterpreter(document);
        List<CurrencyObject> currencyObjects = fileInterpreter.XMLFIleInterpreter(document);

        for (CurrencyObject co : currencyObjects
        ) {
            if (co.kod_waluty.equals(str)) {
                return true;
            }
        }

        return false;
    }
}
