import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class XMLFileGeter {

    public Document getFile(String file) throws ParserConfigurationException, IOException, SAXException {
        String fileLocation = file;

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(fileLocation);

        doc.getDocumentElement().normalize();

        return doc;
    }
}
