import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;

public class XMLFIleInterpreter {

    XMLFileGeter fileGeter = new XMLFileGeter();
    ArrayList<CurrencyObject> currencyObjects = new ArrayList<CurrencyObject>();

    public XMLFIleInterpreter(XMLFileGeter fileGeter) {
        this.fileGeter = fileGeter;
    }


    public XMLFIleInterpreter(Document file) {
    }

    public ArrayList<CurrencyObject> XMLFIleInterpreter(Document doc) throws IOException, SAXException, ParserConfigurationException {

        XMLFileGeter fileGeter = new XMLFileGeter();

        NodeList nodeList = doc.getElementsByTagName("pozycja");

        for (int i = 0; i < nodeList.getLength(); i++) {

            Node node = nodeList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {

                Element element = (Element) node;
                String przelicznik = element.getElementsByTagName("przelicznik").item(0).getTextContent();
                String kod_waluty = element.getElementsByTagName("kod_waluty").item(0).getTextContent();
                String kurs_kupna = element.getElementsByTagName("kurs_kupna").item(0).getTextContent();
                String kurs_sprzedazy = element.getElementsByTagName("kurs_sprzedazy").item(0).getTextContent();
                String nazwa_waluty = element.getElementsByTagName("nazwa_waluty").item(0).getTextContent();

                CurrencyObject currencyObject = new CurrencyObject(nazwa_waluty, przelicznik, kod_waluty, kurs_kupna, kurs_sprzedazy);
                currencyObjects.add(currencyObject);
            }
        }

        return currencyObjects;
    }
}
